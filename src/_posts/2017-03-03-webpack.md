---
layout: post
title:  "Getting Started with Webpack"
date:   2017-03-03 00:00:00 +1200
---
Webpack is a module bundler for modern JavaScript applications. When webpack processes your application, it recursively builds a dependency graph that includes every module your application needs, then packages all of those modules into a small number of bundles - often only one - to be loaded by the browser.

[Getting Started Guide][get-started].

[get-started]: https://webpack.js.org/guides/getting-started/
