---
layout: post
title:  "The History API"
date:   2017-04-04 00:00:00 +1200
---
The DOM `window` object provides access to the browser's history through the [`history`][history] object. It exposes useful methods and properties that let you move back and forth through the user's history, as well as -- starting with HTML5 -- manipulate the contents of the history stack.

`pushState()` takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL. A `popstate` event is dispatched to the window every time the active history entry changes. If the history entry being activated was created by a call to `pushState` or affected by a call to `replaceState`, the `popstate` event's `state` property contains a copy of the history entry's state object.

[Manipulating the browser history][manipulating-history].

[history]: https://developer.mozilla.org/en-US/docs/Web/API/Window/history
[manipulating-history]: https://developer.mozilla.org/en-US/docs/Web/API/History_API#Reading_the_current_state
