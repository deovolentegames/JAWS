---
layout: page
title: About
permalink: /about/
---

I want to make websites using [Jekyll], [Webpack] and a touch of [AJAX]. I want them to be lean, but also to have the option of using large established frameworks if necessary. I want to be able to fix them when things (inevitably) break. I want Jekyll because it has a nice ecosystem available. I want Webpack so that I can compress stuff and replace Jekyll Assets. I want AJAX and the [history API][history-API] so I can do cool stuff without having to reload the web page.

Unfortunately there wasn't an existing boilerplate setup to do exactly what I want, so I had to make my own.

**Check out the [gitlab repo][repo] if you want to see how this works.** I intend to use this as a basis for all the websites I make (minus the styles and animations). If you're looking for an out-of-the-box solution you're probably better off going with [lanyon] and modifying it since this is set up to be quite specific to my use.

## Why make this?

While I probably should've gone with one an existing option there is simply nothing that fulfils all my needs (or wants). I tried alternatives like [Lanyon] and couldn't get them to run. By making something myself I get exactly what I need without all the fluff. Plus it's a great learning experience and I hope it might help others as well.

## Inspiration

Jekyll with Webpack inspired by [allizad] ([medium link][allizad-medium] / [github][allizad-git]), the [jekyll-webpack-starter] and [David Stancu's blog post][david-stancu].

Jekyll with AJAX inspired by [jeykll-ajax] and [Mozilla's article on the history API][history-api].

You can find the source code for Jekyll at GitHub:
[jekyll](https://github.com/jekyll) /
[jekyll](https://github.com/jekyll/jekyll)

You can find the source code for Webpack at GitHub:
[webpack](https://github.com/webpack) /
[webpack](https://github.com/webpack/webpack)

You can find out more about pushstate and the history API at MDN: [Manipulating the browser history][history-example] with a great simple tutorial [here][history-tut].

[jekyll]: https://jekyllrb.com/
[webpack]: https://webpack.js.org/
[ajax]: https://developer.mozilla.org/en-US/docs/AJAX/Getting_Started
[history-api]: https://developer.mozilla.org/en-US/docs/Web/API/History
[repo]: https://gitlab.com/deovolentegames/JAWS
[lanyon]: http://lanyon.io/
[allizad]: http://allizad.com/2016/05/02/using-webpack-with-jekyll/index.html
[allizad-medium]: https://medium.com/@allizadrozny/using-webpack-and-react-with-jekyll-cfe137f8a2cc
[allizad-git]: https://github.com/allizad/jekyll-webpack
[jekyll-webpack-starter]: https://github.com/carsonjones/jekyll-webpack-boilerplate
[david-stancu]: https://davidstancu.me/foss/2017/07/09/building-this-page.html
[jeykll-ajax]: https://github.com/joelhans/Jekyll-AJAX
[history-example]: https://developer.mozilla.org/en-US/docs/Web/API/History_API#Example_of_pushState()_method
[history-tut]: https://css-tricks.com/rethinking-dynamic-page-replacing-content/
