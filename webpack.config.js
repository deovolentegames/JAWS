const path = require("path");
const webpack = require('webpack');

var config = {
  // TODO: Add common Configuration
  entry: "./webpack/index.ts",
  module: {}
};

var productionConfig = Object.assign({}, config, {
  name: "production",
  output: {
    // We’re going to put the generated file in the _assets folder
    // so jekyll-assets will grab it.
    path: __dirname + "/src/_assets/js/",
    filename: "bundle.js"
  },
  resolve: {
      extensions: [".ts"]
  },
  module: {
    rules: [
        {
            test: /\.ts$/,
            use: ["ts-loader"]
        }
    ]
  }
});

var developConfig = Object.assign({}, config,{
  name: "develop",
  output: {
    // We’re going to put the generated file in the assets folder
    // so Jekyll will grab it.
    path: __dirname + "/src/_assets/js/",
    filename: "bundle.js"
  },
  resolve: {
      extensions: [".ts", ".js"]
  },
  // Enable sourcemaps for debugging webpack's output
  devtool: "source-map",
  module: {
    rules: [
      {
          test: /\.ts$/,
          use: ["ts-loader"]
      },
      {
          test: /\.js$/,
          use: ["source-map-loader"],
          enforce: "pre"
      }
    ]
  }
});

// Return Array of Configurations
module.exports = env => {
  console.log('Production: ', env.production)
  if (env.production)
    return productionConfig
  else
    return developConfig
};
