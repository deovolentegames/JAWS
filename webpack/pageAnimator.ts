export function pageAnimator (url: string, homepage: string) {
    if (url === homepage) {
        // set top to scrolled position to prevent weird animations
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        $(".scroll-content").css("top", -scrollTop);

        $(".slide-over").removeClass("hidden");
        $(".stay-under").addClass("hidden");
        $(".nav-fade").addClass("hidden");
        $(".nav-primary").addClass("hidden");
    } else {
        // reset scroll position
        $(".scroll-content").css("top", 0);

        $(".slide-over").addClass("hidden");
        $(".stay-under").removeClass("hidden");
        $(".nav-fade").removeClass("hidden");
        $(".nav-primary").removeClass("hidden");
    }
    // Detecting css transition end might help at some point
    // See:  http://blog.teamtreehouse.com/using-jquery-to-detect-when-css3-animations-and-transitions-end
}
