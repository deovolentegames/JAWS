// TODO: Add timeout.
// TODO: Add popup on fail.
// TODO: Use transitionend to make transform and opacity transitions even more efficient:
// See: https://medium.com/outsystems-experts/how-to-achieve-60-fps-animations-with-css3-db7b98610108

export function setupContentLoader (homepage: string, onLoad: (url: string, data: any) => void) {
    if (!window.history || !window.history.pushState) {
        return;
    }

    $(document).ready(($) => {
        // window.location.href?
        const siteUrl = location.protocol + "//" + (document.location.hostname || document.location.host);

        if ("scrollRestoration" in history) {
            history.scrollRestoration = "manual";
        }

        // Catch internal links
        $(document).delegate("a[href^=\"/\"],a[href^=\"" + siteUrl + "\"]", "click", (e) => {
            // Skip if not clicking normally
            if (e.ctrlKey || e.shiftKey || e.altKey) {
                return true;
            }
            e.preventDefault();
            const pathname = (<HTMLAnchorElement>e.currentTarget).pathname;

            if (location.pathname !== pathname) {
                history.pushState({}, "", pathname);
                load(pathname);
            }
        });

        // Reload content on history state change
        window.addEventListener("popstate", (e) => { load(location.pathname, e.state); });

        // Save scroll position
        // This would be better if it was possible: https://stackoverflow.com/a/4585031
        window.addEventListener("scroll", () => {
            history.replaceState(Object.assign({}, history.state, { scroll: $(document).scrollTop() }), "");
        });

        // Load page content using AJAX
        function load (url: string, stateData: any = {}) {
            $.get(url, (data) => {
                onLoad(url, stateData);
                document.title = $(data).filter("title").first().text();
                $("#navbar").html($(data).find("#navbar").html());
                if (url !== homepage) {
                    $("main").html($(data).find("main").html());
                    if (stateData.scroll) {
                        $(document).scrollTop(stateData.scroll);
                    }
                }
            });
        }
    });
}
