import {setupContentLoader} from "./contentLoader";
import {pageAnimator} from "./pageAnimator";

let homepage = "/JAWS/";

setupContentLoader(homepage,
    (url, stateData) => {
        pageAnimator(url, homepage);
    },
);

// Make animations look a little less janky in Edge/IE
$(document).ready(($) => {
    if (!("scrollRestoration" in history)) {
        $(".slide-over").addClass("no-animate");
        $(".stay-under").addClass("no-animate");
    }
});

// Collapse navbar on click
$(document).click((event) => {
    $(".navbar-collapse").collapse("hide");
});
