# [JAWS]

*Jekyll with AJAX and Webpack. View the site [here][jaws].*

I want to make websites using [Jekyll], [Webpack] and a touch of [AJAX]. I want them to be lean, but also to have the option of using large established frameworks if necessary. I want to be able to fix them when things (inevitably) break. I want Jekyll because it has a nice ecosystem available. I want Webpack so that I can compress stuff and replace Jekyll Assets. I want AJAX and the [history API][history-API] so I can do cool stuff without having to reload the web page.

Unfortunately there wasn't an existing boilerplate setup to do exactly what I want, so I had to make my own.

**Check out the [Gitlab Pages site][jaws] if you want to see this in action.** I intend to use this as a basis for all the websites I make (minus the styles and animations). If you're looking for an out-of-the-box solution you're probably better off going with [lanyon] and modifying it since this is set up to be quite specific to my use.

## Why make this?

While I probably should've gone with one an existing option there is simply nothing that fulfils all my needs (or wants). I tried alternatives like [Lanyon] and couldn't get them to run. By making something myself I get exactly what I need without all the fluff. Plus it's a great learning experience and I hope it might help others as well.

## Inspiration

Jekyll with Webpack inspired by [allizad] ([medium link][allizad-medium] / [github][allizad-git]), the [jekyll-webpack-starter] and [David Stancu's blog post][david-stancu].

Jekyll with AJAX inspired by [jeykll-ajax] and [Mozilla's article on the history API][history].

## Requirements

* Node 7.2.1
* NPM 5.4.2
* Ruby 2.3
* Jekyll 3.5.2
* Bundler 1.15.4

Or at least these are what worked for me.

## How to use

1. Make sure you have [Ruby] and [Node] installed.

1. Make sure you have Jekyll and Bundler installed:
    ```sh
    gem install jekyll bundler
    ```
1. Install packages:
    ```sh
    npm install
    ```
1. Build the website:
    ```sh
    npm build
    ```
    Or if developing then start a server with:
    ```sh
    npm start
    ```
    (If you have Visual Studio Code you can just press `ctrl+shift+B`).

## Issues

* If there are weird dependency errors with ruby gems try `bundle update`.

* If there are weird errors in the development build try `npm run clean`. The repo isn't cleaned automatically when starting dev builds, only production.

## Wot I did

1. Set up this readme, `.vs-code` folder and a basic `.gitignore`.

1. Created new Jekyll site.
    ```sh
    jekyll new . --force
    ```
1. Created base files and reorganised Jekyll scaffold. This includes setting up [Jekyll Assets][jekyll-assets] to serve assets and [Bootstrap] because I don't have time for styling content. See the `Gemfile` and `_config.yml` for all the relevant additions.
1. Initialised npm.
    ```sh
    npm init
    ```
    Then filled out all the relevant info and installed some dev-dependancies so I could build using npm.
    ```sh
    npm install --save-dev cross-env npm-run-all
    ```
    And finally made the scripts. See `package.json` for those.
1. Added [TypeScript] and [tslint]. This step was optional but I prefer writing in TypeScript. Babel would've been good too.
    ```sh
    npm install typescript ts-lint --save-dev
    ```
1. Added Webpack, starting with the `webpack.config.js` file. I needed a few loaders for both TypeScript and source maps.
    ```sh
    npm install webpack ts-loader source-map-loader --save-dev
    ```
    ~~Note: Jekyll Assets doesn't like source maps~~ This is no longer true! Just make sure use Jekyll assets 3+ and Sprockets 4+.

    Finally I rounded it all off with a couple of scripts in `package.json`.
1. Added some AJAX and history API stuff. The code is a mix of what's at [Jekyll AJAX][jeykll-ajax] and [this post from CSS-Tricks][css-tricks-dynamic]. [This newer post about the history API][css-tricks-history] helped too. I just dumped the following into `webpack/index.ts`.
    ```ts
    $(document).ready(($) => {
        const siteUrl = "http://" + (document.location.hostname || document.location.host);

        // Catch internal links
        $(document).delegate("a[href^=\"/\"],a[href^=\"" + siteUrl + "\"]", "click", (e) => {
            // Skip if not clicking normally
            if (e.ctrlKey || e.shiftKey || e.altKey) {
                return true;
            }
            e.preventDefault();
            const pathname = (<HTMLAnchorElement>e.target).pathname;
            history.pushState({}, "", pathname);
            loadContent(pathname);
        });

        // Reload content on history state change
        window.onpopstate = () => {
            loadContent(location.pathname);
        };

        // Load page content using AJAX
        function loadContent (url: string) {
            $.get(url, (data) => {
                document.title = $(data).filter("title").first().text();
                $(".navbar-nav").html($(data).find(".navbar-nav").html());
                $(".main").html($(data).find(".main").html());
            });
        }
    });
    ```
    And it all worked, just like magic.

## BONUS FEATURES

1. I made some minor polish changes and created a simple [Gitlab CI build script][ci-build] to make pushing live builds super easy.

1. I added [BrowserSync] support with a package.json script.
    ```sh
    npm install browser-sync --save-dev
    ```
    ```json
    "scripts": {
        "browser-sync": "browser-sync start --proxy \"localhost:4000\" --host 0.0.0.0 --port 8080 --no-open --files \"public/**/*.*\"",
        ...
    }
    ```
1. I added some CSS transitions, nothing special. I just wanted to do something with the Typescript support.
1. Added a sitemap and robots.txt

## TODO

1. I would like to optimise the site a bit. The CSS transitions could be better.
1. I want to remove some of the junk. I'm using an unoptimised bootstrap install alongside jquery, which isn't really needed.

[jaws]: https://deovolentegames.gitlab.io/JAWS/
[jekyll]: https://jekyllrb.com/
[webpack]: https://webpack.js.org/
[ajax]: https://developer.mozilla.org/en-US/docs/AJAX/Getting_Started
[history]: https://developer.mozilla.org/en-US/docs/Web/API/History
[lanyon]: http://lanyon.io/
[allizad]: http://allizad.com/2016/05/02/using-webpack-with-jekyll/index.html
[allizad-medium]: https://medium.com/@allizadrozny/using-webpack-and-react-with-jekyll-cfe137f8a2cc
[allizad-git]: https://github.com/allizad/jekyll-webpack
[jekyll-webpack-starter]: https://github.com/carsonjones/jekyll-webpack-boilerplate
[david-stancu]: https://davidstancu.me/foss/2017/07/09/building-this-page.html
[jeykll-ajax]: https://github.com/joelhans/Jekyll-AJAX
[history-api]: https://developer.mozilla.org/en-US/docs/Web/API/History_API
[ruby]: https://rubyinstaller.org/
[node]: https://nodejs.org/en/download/current/
[jekyll-assets]: https://github.com/jekyll/jekyll-assets
[bootstrap]: http://getbootstrap.com/
[typescript]: https://www.typescriptlang.org/
[tslint]: https://palantir.github.io/tslint/
[css-tricks-dynamic]: https://css-tricks.com/rethinking-dynamic-page-replacing-content/
[css-tricks-history]: https://css-tricks.com/using-the-html5-history-api/
[ci-build]: https://gitlab.com/deovolentegames/JAWS/blob/develop/.gitlab-ci.yml
[browsersync]: https://browsersync.io/
